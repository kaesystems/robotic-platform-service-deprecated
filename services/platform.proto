syntax = "proto3";

package robot;
option go_package = "bitbucket.com/kaesystems/robotic-platform-service/api";
// The following message will be used for 
message Empty{

}

// Used for fleet ops
enum Operation {
    Create = 0;
    Update = 1;
    Delete = 2;
}

message FleetRequest {
    string name= 1;
}


message FleetList {
    FleetRequest fleet = 1;
    Operation operation = 2;
}

message FleetResponse {
    string name= 1;
    repeated RobotRequest robots = 2; 

}

message Env {
    string name =1;
    string value = 2;
}
message Launch {
    repeated Env env_vars = 1;
    string launch_file_path=2;
}

message RobotRequest {
    string name = 1;
    string distro =2;
    bool cloud_ide = 3;
    string workspaces_path= 4;
    repeated Workspace workspaces = 5;
    bool access= 6;
    FleetRequest fleet = 7;
    string cluster_name =8;
}

message GetRobotRequest {
    string name = 1;
    FleetRequest fleet = 6;
}


message RobotConfigStatus {
    bool created= 1;
    string phase =2;
}

message RobotRuntimeStatus {
    message PeebleServices {
        string name =1;
        string current = 2;
        string startup = 3;
    }

    message EphemeralContainer {
        string name = 1;
        string phase = 2;
        repeated PeebleServices peeble_service_statuses = 3;
    }
    message ServiceStatus{
        bool created = 1;
        string phase = 2; 
    }
    message Node {
        string executable =1;
        string node_ID=2;
        string node_type =3;
        string package =4;
        int64 peeble_port = 5; 
        bool started = 6;
        int32 index = 7;
        EphemeralContainer ephemeral_container = 8;
        
    }
    message BoolStatus {
        bool created= 1;
    }
    message RbacStatus {
        BoolStatus service_account = 1;
        BoolStatus role = 2;
        BoolStatus role_binding=3;
    }
    message RosPodStatus {
        bool created = 1;
        string ip = 2;
        string phase =3;
    }
    message AgentStatus {
        bool created = 1;
        string phase =2;
        repeated Node nodes =3;
    }
    message Status {
        BoolStatus peeble_config_status = 1;
        RbacStatus rbac_status =2;
        BoolStatus ros_service_status=3;
        RosPodStatus ros_pod_status=4;
        ServiceStatus node_manager_status=5;
        AgentStatus agent_status =6;
    }
    bool created = 1;
    string phase =2;
    Status status = 3;
}

enum BuildTypes {
    BuildStandart = 0;
    BuildCustom = 1;
}

message Repository {
    string name = 1;
    string url = 2;
    string branch = 3;
    Launch launch = 4;

}
message Step {
string name = 1;
string command = 2;
string script = 3;
}
message Workspace {
    string name =1 ;
    string sub_path=3;
    string build = 4;
    repeated Step build_steps = 5;
    repeated Repository repositories = 6 ;
}

message RobotResponse {
    string name = 1;
    string distro =2;
    bool cloud_ide = 3;
    string workspaces_path= 4;
    repeated Workspace workspaces = 5;
    bool access= 6;
    FleetRequest fleet = 7;
    string cluster_name =8;
    RobotRuntimeStatus robot_runtime_status = 9;
    RobotConfigStatus robot_config_status =10;

}

message ListRobotResponse {
    message RobotSummary {
        string name = 1;
        string distro =2;
        string cluster_name =4;
    }
    RobotSummary robot=1;
    Operation operation = 2;

}

message RouterSettings{
    message Allowance{
        string name = 1;
        string type = 2;
    }
    message Endpoint{
        message ListeningAddress {
            string ip = 1;
            int64 port = 2; 
            string transport = 3;
        }
        int64 id =1;
        string type = 2;
        bool rosDiscoveryServer = 3;
        repeated ListeningAddress listeningAddresses = 4;
    }
    Allowance allowlist = 1; 
    Endpoint LocalDiscoveryServer = 2;
    Endpoint CloudWAN = 3;
}


message GetRouterRequest {
    string name = 1;
    FleetRequest fleet = 6;
}

message Router{
    message Allowance{
        string name = 1;
        string type = 2;
    }
    message ListeningAddress {
            string ip = 1;
            int64 port = 2; 
            string transport = 3;
        }
    message CloudPoints{
        int64 id =1;
        string type = 2;
        bool ros_discovery_server = 3;
        repeated ListeningAddress listening_addresses = 4;
    }
    string name = 1;
    FleetRequest fleet = 2;
    repeated Allowance allowlist = 3;
    CloudPoints LocalDiscoveryServer =4;
    CloudPoints CloudWAN = 5;

}

message RouterSummary {
    message Router{
        string Name      = 1;
        string Fleet     =2;
        string InternalIP = 3; 
        string ExternalIP = 4; 
    }
	Router router = 1;
    Operation operation =2;
}


message Observer {
    int32 domain_id = 1;
    string name = 2;
    FleetRequest fleet= 3;
    string elasticsearch_url = 4;
    int32 prometheus_port = 5;
    repeated string filter = 6;
    bool default_filter = 7;  
}

message GetObserverRequest {
    string name = 1;
    FleetRequest fleet = 2;
}

message ClusterRequest{
    string name = 1; 
}
message ClusterWorkflowTransaction{
    string name = 1;
    string run_id =2 ;
}

message ClusterResponse{
    string name = 1;
    string ip = 2;
    string status = 3; 
}

message ClusterResponseSummary{
    ClusterResponse cluster = 1;
    Operation operation = 3;

}

service Robot{
    rpc CreateFleet(FleetRequest) returns (FleetResponse) {}
    rpc DeleteFleet(FleetRequest) returns (Empty) {}
    rpc GetFleet(FleetRequest) returns (FleetResponse) {}
    rpc ListFleets (Empty) returns (stream FleetList) {}
    rpc CreateRobot (RobotRequest) returns (RobotRequest) {}
    rpc GetRobot (GetRobotRequest) returns (RobotResponse) {}
    rpc UpdateRobot (RobotRequest) returns (RobotResponse) {}
    rpc DeleteRobot (GetRobotRequest) returns (Empty) {}
    rpc ListRobots (FleetRequest) returns (stream ListRobotResponse) {}
    rpc CreateRouter (Router) returns (Router) {}
    //Not have logic
    rpc ListRouter (FleetRequest) returns (stream RouterSummary) {}

    rpc GetDefaultRouter(FleetRequest) returns (Router) {}
    rpc GetRouter (GetRouterRequest) returns (Router) {}
    rpc DeleteRouter (GetRouterRequest) returns (Empty) {}
    rpc UpdateRouter (Router) returns (Router) {}
    rpc CreateObserver (Observer) returns (Observer) {}
    rpc GetObserver (GetObserverRequest) returns (Observer) {}
    rpc UpdateObserver (Observer) returns (Observer) {}
    rpc DeleteObserver(GetObserverRequest) returns (Empty) {}
    rpc CreateCluster(ClusterRequest) returns (ClusterWorkflowTransaction) {}
    rpc GetCluster(ClusterWorkflowTransaction) returns (ClusterResponse) {}
    rpc DeleteCluster(ClusterRequest) returns (Empty) {}
    rpc ListClusters(Empty) returns (stream ClusterResponseSummary) {}
}




