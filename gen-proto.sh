cd services;
protoc --go_out=../api --go_opt=paths=source_relative \
    --go-grpc_out=../api --go-grpc_opt=paths=source_relative \
    platform.proto

protoc --go_out=../../test-client/api --go_opt=paths=source_relative \
    --go-grpc_out=../../test-client/api --go-grpc_opt=paths=source_relative \
    platform.proto