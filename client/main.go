package main

import (
	"context"

	wf "bitbucket.com/kaesystems/robotic-platform-service/pkg/workflow"
	"go.temporal.io/sdk/client"
)

func main() {
	temporalClient, err := client.NewClient(client.Options{
		// HostPort: "100.25.198.115:31255",
		HostPort: "34.224.78.190:30494", // thats main cluster temporal IP

	})
	if err != nil {
		panic(err)
	}
	defer temporalClient.Close()
	// ...
	workflowOptions := client.StartWorkflowOptions{
		ID:        "test-case",
		TaskQueue: wf.VirtualClusterQueue,
	}
	_, err = temporalClient.ExecuteWorkflow(context.Background(), workflowOptions, wf.CreateVirtualCluster, wf.VirtualClustersParamater{
		Name:            "vc-5",
		Username:        "engin",
		ClusterTemplate: "keycloak-cluster",
	})
	if err != nil {
		panic(err)
	}
	// fmt.Printf("Workflow ID: %v\n", workflowRun.GetID())
	// fmt.Printf("Workflow Run ID: %v\n", workflowRun.GetRunID())
}

//This worker should work seperately!
