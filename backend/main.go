package main

import (
	"log"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/handler"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/settings"
	"github.com/spf13/viper"
)

func main() {
	//TODO: Configuration section!!

	if err := settings.ReadSettings(); err != nil {
		panic(err)
	}

	log.Printf("Cluster Settings readed from %v", viper.Get("configSource"))
	log.Printf("Server is started from 0.0.0.0%v\n", handler.PORT)

	handler.StartServer()
}
