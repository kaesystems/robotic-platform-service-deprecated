package store

import (
	"github.com/go-redis/redis"
)

func SaveIP(ip string) error {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "100.25.198.115:32542",
		Password: "admin", // no password set
		DB:       0,       // use default DB
	})
	// var ctx = context.Background()

	err := rdb.Set(ip, "true", 0).Err()
	if err != nil {
		panic(err)
	}
	return nil

}

func GetIP(ip string) (string, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "100.25.198.115:32542",
		Password: "admin", // no password set
		DB:       0,       // use default DB
	})

	val, err := rdb.Get(ip).Result()
	if err != nil {
		return "", err
	}
	return val, nil
}
