package store_test

import (
	"testing"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/store"
)

func TestSaveIP(t *testing.T) {
	const mockIP = "10.0.0.0"
	store.SaveIP(mockIP)

	data, err := store.GetIP(mockIP)
	if err != nil {
		panic(err)
	}
	if data != "true" {
		t.Fail()
	}
}
