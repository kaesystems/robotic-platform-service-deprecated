package settings

import (
	"errors"
	"io/ioutil"

	"github.com/spf13/viper"
	"k8s.io/apimachinery/pkg/util/yaml"
)

type SettingFile struct {
	KubernetesHost  string `yaml:"kubernetesHost"`
	KubernetesToken string `yaml:"kubernetesToken"`
	CaPath          string `yaml:"caPath"`
}

func readSettings() (*SettingFile, error) {
	var conf *SettingFile
	file, err := ioutil.ReadFile("settings.yaml")
	if err != nil {

		return nil, errors.New("settings file not found skipped")
	}
	if err := yaml.Unmarshal(file, &conf); err != nil {
		return nil, err
	}
	return conf, nil
}

func (c *SettingFile) GetKubernetesHost() *string {
	return &c.KubernetesHost
}

func (c *SettingFile) GetKubernetesToken() *string {
	return &c.KubernetesToken
}

func (c *SettingFile) GetCAPath() *string {
	return &c.CaPath
}

func GetConfig() (*SettingFile, error) {
	conf, err := readSettings()

	if err != nil {
		return nil, err
	}
	return conf, nil

}

type ConfigSource string

const ServiceAccount ConfigSource = "serviceAccount"
const ConfigFile ConfigSource = "configFile"

func ReadSettings() error {
	viper.SetConfigName("settings")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	// viper.AddConfigPath("../../.")
	// viper.AddConfigPath("../.")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			viper.Set("configSource", "serviceAccount")
			return nil
		} else {
			return err
		}
	}
	viper.Set("configSource", "configFile")
	return nil
}
