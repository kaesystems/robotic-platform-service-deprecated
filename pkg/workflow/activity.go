package workflow

import (
	"errors"
	"fmt"
	"strconv"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/account"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

//Description: Deploy virtual cluster from given cluster version template. This activity needs admin privilage on cluster.
// func ToClusterKey(vc *v1alpha1.VirtualCluster) string {
// 	// If the ClusterNamespace is set then this will automatically return that prefix allowing us to override
// 	// any other hooks for the ClusterNamespace.
// 	if vc.Status.ClusterNamespace != "" {
// 		return vc.Status.ClusterNamespace
// 	}
// 	digest := sha256.Sum256([]byte(vc.GetUID()))
// 	return vc.GetNamespace() + "-" + hex.EncodeToString(digest[0:])[0:6] + "-" + vc.GetName()
// }
var VirtualCluster = schema.GroupVersionResource{Group: "tenancy.x-k8s.io", Version: "v1alpha1", Resource: "virtualclusters"}

const VirtualClusterQueue = "VIRTUAL_CLUSTER_QUEUE"

func DeployVirtualCluster(param VirtualClustersParamater) (string, error) {
	k, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		return "", err
	}
	vc, err := k.CreateVirtualCluster(GetVirtualCluster(param))
	if err != nil {
		return "", err
	}

	namespace := ToClusterKey(vc)

	//Return Node Port of virtual cluster thats mean that virtual cluster are ready to use
	return namespace, nil
}

func CheckStatusVirtualCluster(param VirtualClustersParamater) (string, error) {
	k, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		return "Cannot reach kubernetes cluster", err
	}
	vc, err := k.GetVirtualCluster(param.Name)
	if err != nil {
		return "", err
	}

	if vc.Object["status"].(map[string]interface{})["phase"] != "Running" {
		return "", errors.New("virtual cluster is not ready")
	}
	return vc.GetResourceVersion(), nil
}

//Description: Create admin role on new cluster. That role can be handled with virtual cluster admin role.
func DeployAdminRole(clusterNamespace string) error {
	fmt.Printf("\nThis a namespace of main cluster: %v\n\n", clusterNamespace)
	client, err := cluster.GetKubernetesAdmin()
	if err != nil {
		return err
	}

	k := client.GetInternalClient()

	svcPort, err := GetServicePort(k, clusterNamespace)
	if err != nil {
		return err
	}

	rawConfig, err := GetVSConfig(k, clusterNamespace)
	if err != nil {
		return err
	}

	vsConfig, err := ReadKubeconfig(rawConfig, "https://34.224.78.190:"+strconv.Itoa(int(svcPort)))
	if err != nil {
		return err
	}

	vsClient, err := GetAdminClient(vsConfig)
	if err != nil {
		return err
	}

	if err := CreateRoleBinding(vsClient, clusterNamespace); err != nil {
		return err
	}

	return nil
}

//Description: Assign role to the keycloak user.
func AssignRoleToUser(roleName string, username string) error {
	//FIXME: Keycloak method should configurable and secure
	kclient, err := account.GetKeycloak("https://identity.robolaunch.cloud", "kubernetes-platform", "master", "admin", "admin")
	if err != nil {
		return err
	}
	if err := kclient.CreateGroup(roleName); err != nil {
		return err
	}
	if err := kclient.BindGroup(username, roleName); err != nil {
		return err

	}
	return nil
}

//Description: Create and assign subnet to new virtual clusters namespace from main cluster. This activity needs admin privilage on cluster.
func CreateSubnet(vsName string, namespaces []string) error {
	//FIXME: Create random ip

	k, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		return err
	}
	ip, err := k.GenerateIP()
	if err != nil {
		return err
	}
	if err := k.CreateSubnet(GetSubnet(vsName, namespaces, *ip)); err != nil {
		return err
	}
	return nil
}
