package workflow

import (
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/ipservice"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetVirtualCluster(param VirtualClustersParamater) map[string]interface{} {
	return map[string]interface{}{
		"apiVersion": "tenancy.x-k8s.io/v1alpha1",
		"kind":       "VirtualCluster",
		"metadata": map[string]interface{}{
			"name": param.Name,
		},
		"spec": map[string]interface{}{
			"clusterDomain":      "cluster.local",
			"clusterVersionName": param.ClusterTemplate,
			"pkiExpireDays":      365,
			"opaqueMetaPrefixes": []string{
				"tenancy.x-k8s.io",
			},
			"transparentMetaPrefixes": []string{
				"k8s.net.status",
			},
		},
	}
}

func GetSubnet(vsName string, namespaces []string, ip ipservice.IP) map[string]interface{} {
	excludeIP := ip.CopyIP()
	excludeIP.FourthBlock = 1
	return map[string]interface{}{
		"apiVersion": "kubeovn.io/v1",
		"kind":       "Subnet",
		"metadata": map[string]interface{}{
			"name": "subnet-" + vsName,
		},
		"spec": map[string]interface{}{
			"provider":   "ovn",
			"protocol":   "IPv4",
			"namespaces": namespaces,
			"acls": []map[string]interface{}{
				{
					"action":    "allow",
					"direction": "to-lport",
					"match":     "ip4.src==" + ip.GetIPBlock() + " && ip4.dst==" + ip.GetIPBlock(),
					"priority":  2201,
				},
				{
					"action":    "reject",
					"direction": "to-lport",
					"match":     "ip4.src==10.10.0.0/16 && ip4.dst==10.10.0.0/16",
					"priority":  2101,
				},
				{
					"action":    "allow",
					"direction": "to-lport",
					"match":     "ip4.src==0.0.0.0/0 && ip4.dst==" + ip.GetIPBlock(),
					"priority":  2001,
				},
				{
					"action":    "allow",
					"direction": "to-lport",
					"match":     "ip4.src==" + ip.GetIPBlock() + " && ip4.dst==0.0.0.0/0",
					"priority":  2002,
				},
			},
			"cidrBlock": ip.GetIPBlock(),
			"excludeIps": []string{
				excludeIP.GetIP(),
			},
			"default":     false,
			"private":     true,
			"natOutgoing": true,
			"gatewayType": "distributed",
		},
	}
}

func GetClusterRoleBinding(vsName string) *rbacv1.ClusterRoleBinding {
	return &rbacv1.ClusterRoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name: "default-admin",
		},
		RoleRef: rbacv1.RoleRef{
			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "ClusterRole",
			Name:     "admin",
		},
		Subjects: []rbacv1.Subject{
			{
				APIGroup: "rbac.authorization.k8s.io",
				Kind:     "Group",
				Name:     vsName + "admin",
			},
		},
	}
}
