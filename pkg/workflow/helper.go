package workflow

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"os/exec"

	"gopkg.in/yaml.v3"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type VSConfig struct {
	Token string
	Cert  string
	Key   string
}

func ToClusterKey(vc *unstructured.Unstructured) string {
	// If the ClusterNamespace is set then this will automatically return that prefix allowing us to override
	// any other hooks for the ClusterNamespace.
	// if vc.Object["status"].(map[string]interface{})["clusterNamespace"] != "" {
	// 	return vc.Object["status"].(map[string]interface{})["clusterNamespace"].(string)
	// }
	digest := sha256.Sum256([]byte(vc.GetUID()))
	return vc.GetNamespace() + "-" + hex.EncodeToString(digest[0:])[0:6] + "-" + vc.GetName()
}

func GetServicePort(client kubernetes.Interface, clusterNamespace string) (int32, error) {
	svc, err := client.CoreV1().Services(clusterNamespace).Get(context.TODO(), "apiserver-svc", metav1.GetOptions{})
	if err != nil {
		return 0, err
	}
	return svc.Spec.Ports[0].NodePort, nil
}

func GetVSConfig(client kubernetes.Interface, clusterNamespace string) ([]byte, error) {
	const secretName string = "admin-kubeconfig"
	secret, err := client.CoreV1().Secrets(clusterNamespace).Get(context.TODO(), "admin-kubeconfig", metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	encoded, exist := secret.Data[secretName]
	if !exist {
		return nil, fmt.Errorf("failed to get data from secret")
	}
	return encoded, nil
}

func ReadKubeconfig(data []byte, host string) (*rest.Config, error) {
	kubeconfig := make(map[string]interface{})
	if err := yaml.Unmarshal(data, &kubeconfig); err != nil {
		return nil, err
	}
	certData, err := base64.StdEncoding.DecodeString(fmt.Sprintf("%v", kubeconfig["users"].([]interface{})[0].(map[string]interface{})["user"].(map[string]interface{})["client-certificate-data"]))
	if err != nil {
		return nil, err
	}
	keyData, err := base64.StdEncoding.DecodeString(fmt.Sprintf("%v", kubeconfig["users"].([]interface{})[0].(map[string]interface{})["user"].(map[string]interface{})["client-key-data"]))
	if err != nil {
		return nil, err
	}
	// FIXME: probably not worked because of cert is not created for ElasticIP
	// caData, err := base64.StdEncoding.DecodeString(fmt.Sprintf("%v", kubeconfig["clusters"].([]interface{})[0].(map[string]interface{})["cluster"].(map[string]interface{})["certificate-authority-data"]))
	// if err != nil {
	// 	return nil, err
	// }
	var config = &rest.Config{
		Host: host,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
			CertData: certData,
			KeyData:  keyData,
			// CAData:   caData,
		},
	}
	return config, nil

}

func GetAdminClient(config *rest.Config) (*kubernetes.Clientset, error) {
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func CreateRoleBinding(client *kubernetes.Clientset, vsName string) error {
	crb := GetClusterRoleBinding(vsName)
	_, err := client.RbacV1().ClusterRoleBindings().Create(context.TODO(), crb, metav1.CreateOptions{})
	if err != nil {
		return err
	}
	return nil
}

// The given function create random psk
func CreatePSK() (string, error) {
	cmd := exec.Command("/bin/bash", "-c", "tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w 64 | head -n 1")
	stdout, err := cmd.Output()
	if err != nil {
		return "err", err
	}
	return string(stdout), nil
}
