package workflow

import (
	"time"

	"go.temporal.io/sdk/temporal"
	"go.temporal.io/sdk/workflow"
)

type VirtualClustersParamater struct {
	Name            string
	Username        string
	ClusterTemplate string
}

func CreateVirtualCluster(ctx workflow.Context, params VirtualClustersParamater) error {
	retrypolicy := &temporal.RetryPolicy{
		InitialInterval:    30 * time.Second,
		BackoffCoefficient: 1.0,
		MaximumInterval:    30 * time.Second, // 100 * InitialInterval
		MaximumAttempts:    5,                // Unlimited
	}
	options := workflow.ActivityOptions{
		ScheduleToCloseTimeout: 5 * time.Minute,
		RetryPolicy:            retrypolicy,
	}

	ctx = workflow.WithActivityOptions(ctx, options)
	var mainNamespace string
	var status string
	var errPointer error

	if err := workflow.ExecuteActivity(ctx, DeployVirtualCluster, params).Get(ctx, &mainNamespace); err != nil {
		return err
	}
	if err := workflow.ExecuteActivity(ctx, CheckStatusVirtualCluster, params).Get(ctx, &status); err != nil {
		return err
	}
	if err := workflow.ExecuteActivity(ctx, DeployAdminRole, mainNamespace).Get(ctx, &errPointer); err != nil {
		return err
	}
	if err := workflow.ExecuteActivity(ctx, AssignRoleToUser, mainNamespace+"-admin", params.Username).Get(ctx, &errPointer); err != nil {
		return err
	}
	//Test namespace example
	nsList := []string{
		mainNamespace + "-default",
		mainNamespace + "-kube-node-lease",
		mainNamespace + "-kube-system",
		mainNamespace + "-kube-public",
	}
	if err := workflow.ExecuteActivity(ctx, CreateSubnet, params.Name, nsList).Get(ctx, &errPointer); err != nil {
		return err
	}
	return nil
}
