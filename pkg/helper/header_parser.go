package helper

import (
	"encoding/base64"
	"encoding/json"
	"errors"

	"google.golang.org/grpc/metadata"
)

type UserInfo struct {
	Username string `json:"preferred_username"`
}

func ParseUserInfo(headers metadata.MD) (*UserInfo, error) {
	if headers["x-jwt"][0] == "" {
		return nil, errors.New("parsed token is empty")
	}
	decoded, err := base64.StdEncoding.DecodeString(headers["x-jwt"][0])
	if err != nil {
		return nil, err
	}
	var user = UserInfo{}
	err = json.Unmarshal(decoded, &user)
	if err != nil {
		return nil, err
	}
	return &UserInfo{}, nil
}
