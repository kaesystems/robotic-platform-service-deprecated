package account

import (
	"context"
	"errors"

	"github.com/Nerzal/gocloak/v10"
)

type Keycloak struct {
	realm      string
	adminRealm string
	client     gocloak.GoCloak
	user       string
	password   string
}

func GetKeycloak(url string, realm string, adminRealm string, username string, password string) (*Keycloak, error) {
	if url == "" || realm == "" || username == "" {
		return nil, errors.New("error happend")
	}
	var keycloak Keycloak
	keycloak.client = gocloak.NewClient(url)
	keycloak.adminRealm = adminRealm
	keycloak.realm = realm
	keycloak.password = password
	keycloak.user = username

	return &keycloak, nil
}

func (k *Keycloak) loginAdmin() (*string, error) {
	ctx := context.Background()
	token, err := k.client.LoginAdmin(ctx, k.user, k.password, k.adminRealm)
	if err != nil {
		return nil, err
	}
	return &token.AccessToken, nil
}

func (k *Keycloak) CreateGroup(groupName string) error {
	ctx := context.Background()
	token, err := k.loginAdmin()
	if err != nil {
		return err
	}
	_, err = k.client.CreateGroup(ctx, *token, k.realm, gocloak.Group{
		Name: gocloak.StringP(groupName),
	})
	if err != nil {
		return err
	}
	return nil
}

func (k *Keycloak) BindGroup(username string, groupName string) error {
	ctx := context.Background()
	token, err := k.loginAdmin()
	if err != nil {
		return err
	}
	user, err := k.client.GetUsers(ctx, *token, k.realm, gocloak.GetUsersParams{
		Username: gocloak.StringP(username),
	})
	if err != nil {
		return err
	}
	group, err := k.client.GetGroups(ctx, *token, k.realm, gocloak.GetGroupsParams{
		Search: gocloak.StringP(groupName),
	})
	if err != nil {
		return err
	}
	err = k.client.AddUserToGroup(ctx, *token, k.realm, *user[0].ID, *group[0].ID)
	if err != nil {
		return err
	}
	return nil
}
