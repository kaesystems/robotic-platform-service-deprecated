package cluster

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	types "bitbucket.org/kaesystems/dds-controller/api/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/tools/cache"
)

var DDSRouterGVR = schema.GroupVersionResource{Group: "dds.roboscale.io", Version: "v1", Resource: "ddsrouters"}

type DDSRouterMessage struct {
	Name       string
	Fleet      string
	InternalIP string
	ExternalIP string
	Operation  platform.Operation
}

func (k *KubeopsDynamic) CreateDDSRouter(object *types.DDSRouter) error {
	//Add type information to fix
	object.TypeMeta = metav1.TypeMeta{
		APIVersion: "dds.roboscale.io/v1",
		Kind:       "DDSRouter",
	}

	var inInterface map[string]interface{}

	inrec, err := json.Marshal(object)
	if err != nil {
		return err
	}
	json.Unmarshal(inrec, &inInterface)

	routerObject := &unstructured.Unstructured{
		Object: inInterface,
	}
	fmt.Println(routerObject)
	_, err = k.client.Resource(DDSRouterGVR).Namespace(object.Namespace).Create(context.TODO(), routerObject, metav1.CreateOptions{})
	if err != nil {
		return err

	}
	return nil

}

func (k *KubeopsDynamic) GetDDSRouter(name string, namespace string) (*types.DDSRouter, error) {
	ctx := context.TODO()
	obj, err := k.client.Resource(DDSRouterGVR).Namespace(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	router := &types.DDSRouter{}
	j, err := json.Marshal(obj.Object)
	if err != nil {
		log.Printf("Error happend: %v", err)
	}
	json.Unmarshal(j, &router)
	if err != nil {
		return nil, err
	}
	return router, nil
}

func (k *KubeopsDynamic) GetDefaultDDSRouter(namespace string) (*types.DDSRouter, error) {
	ctx := context.TODO()
	obj, err := k.client.Resource(DDSRouterGVR).Namespace(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	if len(obj.Items) <= 0 {
		return nil, fmt.Errorf("%v", "object not found in given array")
	}
	router := &types.DDSRouter{}
	j, err := json.Marshal(obj.Items[0].Object)
	if err != nil {
		log.Printf("Error happend: %v", err)
	}
	json.Unmarshal(j, &router)
	if err != nil {
		return nil, err
	}
	return router, nil
}

func (k *KubeopsDynamic) ListDDSRouters(ch chan DDSRouterMessage, fleetName string) error {
	factory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(k.client, time.Minute, fleetName, nil)
	informer := factory.ForResource(DDSRouterGVR).Informer()

	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			router := &types.DDSRouter{}
			j, err := json.Marshal(obj.(*unstructured.Unstructured).Object)
			if err != nil {
				log.Printf("Error happend: %v", err)
			}
			json.Unmarshal(j, &router)
			ch <- DDSRouterMessage{
				Name:       router.GetName(),
				Fleet:      router.GetNamespace(),
				InternalIP: router.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses[0].Ip,
				ExternalIP: router.Spec.RouterSettings.CloudWAN.ListeningAddresses[0].Ip,
				Operation:  platform.Operation_Create,
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			router := &types.DDSRouter{}
			j, err := json.Marshal(new.(*unstructured.Unstructured).Object)
			if err != nil {
				log.Printf("Error happend: %v", err)
			}
			json.Unmarshal(j, &router)
			ch <- DDSRouterMessage{
				Name:       router.GetName(),
				Fleet:      router.GetNamespace(),
				InternalIP: router.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses[0].Ip,
				ExternalIP: router.Spec.RouterSettings.CloudWAN.ListeningAddresses[0].Ip,
				Operation:  platform.Operation_Update,
			}
		},
		DeleteFunc: func(obj interface{}) {
			router := &types.DDSRouter{}
			j, err := json.Marshal(obj.(*unstructured.Unstructured).Object)
			if err != nil {
				log.Printf("Error happend: %v", err)
			}
			json.Unmarshal(j, &router)

			ch <- DDSRouterMessage{
				Name:       router.GetName(),
				Fleet:      router.GetNamespace(),
				InternalIP: router.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses[0].Ip,
				ExternalIP: router.Spec.RouterSettings.CloudWAN.ListeningAddresses[0].Ip,
				Operation:  platform.Operation_Delete,
			}
		},
	})
	stop := make(chan struct{})
	go informer.Run(stop)
	for {
		time.Sleep(time.Second)
	}
}

func (k *KubeopsDynamic) UpdateDDSRouter(router *types.DDSRouter) error {
	ctx := context.TODO()

	obj, err := k.client.Resource(DDSRouterGVR).Namespace(router.Namespace).Get(ctx, router.Name, metav1.GetOptions{})
	if err != nil {
		log.Println("main error!!")
		return err
	}
	var spec map[string]interface{}
	var metadata map[string]interface{}
	log.Println(router)
	if err := unstructuredDecoder(&router.Spec, &spec); err != nil {
		log.Printf("spec transformation error:%v", err)
		return err
	}
	if err := unstructuredDecoder(&router.ObjectMeta, &metadata); err != nil {
		log.Printf("metadata transformation error:%v", err)
		return err
	}
	obj.Object["spec"] = spec
	obj.Object["metadata"].(map[string]interface{})["annotations"] = metadata["annotations"]
	obj.Object["metadata"].(map[string]interface{})["labels"] = metadata["labels"]

	_, err = k.client.Resource(DDSRouterGVR).Namespace(router.Namespace).Update(ctx, obj, metav1.UpdateOptions{})
	if err != nil {
		log.Println("update error!")
		return err
	}
	return nil
}

func (k *KubeopsDynamic) DeleteDDSRouter(name string, namespace string) error {
	err := k.client.Resource(DDSRouterGVR).Namespace(namespace).Delete(context.TODO(), name, metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	return nil
}
