package cluster_test

import (
	"testing"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
)

func TestParseIP(t *testing.T) {
	ip, err := cluster.ParseIp("1.1.1.1/24")
	if err != nil {
		panic(err)
	}
	if ip.FirstBlock != 1 || ip.SecondBlock != 1 || ip.ThirdBlock != 1 || ip.FourthBlock != 1 {
		t.Fail()
	}
}

func TestRandomGenerator(t *testing.T) {
	var exclude []int
	for i := 1; i < 15; i++ {
		exclude = append(exclude, i)
	}
	for i := 0; i < 50; i++ {
		number, err := cluster.GetIPWithExclusion(exclude)
		if err != nil {
			panic(err)
		}
		println(number)
	}
	t.Fail()
}
