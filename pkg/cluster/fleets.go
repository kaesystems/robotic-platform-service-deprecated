package cluster

import (
	"context"
	"fmt"
	"time"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"

	"k8s.io/client-go/tools/cache"
)

type Kubeops struct {
	client *kubernetes.Clientset
}

// Interface for CRD Type resources
type KubeopsDynamic struct {
	client dynamic.Interface
}

type FleetMessage struct {
	FleetName string
	Operation platform.Operation
}

// type Kubeops struct {
// 	namespace string `json:"name"`
// }

//TODO: Create namespace role managed by admin

func GetLabel() string {

	fleetReq, _ := labels.NewRequirement("robolaunch.io/type", selection.Equals, []string{"fleet"})

	selector := labels.NewSelector()
	selector = selector.Add(*fleetReq)
	return selector.String()
}

func isFleet(labels map[string]string) bool {
	for key, value := range labels {
		if key == "robolaunch.io/type" && value == "fleet" {
			return true
		}
	}
	return false
}

func (k *Kubeops) CreateNamespace(name string) error {
	ns := corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"robolaunch.io/fleet": name,
				"robolaunch.io/type":  "fleet",
			},
		},
	}
	_, err := k.client.CoreV1().Namespaces().Create(context.TODO(), &ns, metav1.CreateOptions{})
	if err != nil {
		return err
	}
	return nil
}

func (k *Kubeops) DeleteNamespace(name string) error {
	//Get Namespaces from Cluster
	fleet, err := k.client.CoreV1().Namespaces().Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		return err
	}

	//Identify given name of the namespace is a valid Robolaunch Fleet
	if !isFleet(fleet.Labels) {
		return fmt.Errorf("%v", "invalid fleet it is a name of the kubernetes namespace")
	}

	//If it is a valid fleet name, remove from the cluster.
	if err := k.client.CoreV1().Namespaces().Delete(context.TODO(), name, metav1.DeleteOptions{}); err != nil {
		return err
	}
	return nil
}

func (k *Kubeops) ListNamespace(name string, ch chan FleetMessage) error {

	modifier := func(options *metav1.ListOptions) {
		options.LabelSelector = GetLabel()
	}

	watchlist := cache.NewFilteredListWatchFromClient(
		k.client.CoreV1().RESTClient(),
		string("namespaces"),
		corev1.NamespaceAll,
		modifier,
	)

	_, controller := cache.NewInformer(
		watchlist,
		&corev1.Namespace{},
		0,
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				ch <- FleetMessage{
					FleetName: obj.(*corev1.Namespace).Name,
					Operation: platform.Operation_Create,
				}
			},
			UpdateFunc: func(old interface{}, new interface{}) {
				// fmt.Printf("Object is: %v chaned with %v", old.(*corev1.Namespace).Name, new.(*corev1.Namespace).Name)

			},
			DeleteFunc: func(obj interface{}) {
				ch <- FleetMessage{
					FleetName: obj.(*corev1.Namespace).Name,
					Operation: platform.Operation_Delete,
				}
			},
		},
	)
	stop := make(chan struct{})
	go controller.Run(stop)
	for {
		time.Sleep(time.Second)
	}
}
