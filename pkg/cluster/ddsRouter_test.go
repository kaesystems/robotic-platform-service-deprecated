package cluster_test

import (
	"reflect"
	"testing"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/settings"
	types "bitbucket.org/kaesystems/dds-controller/api/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var mockRouter = &types.DDSRouter{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "router-example",
		Namespace: "robosample",
	},
	Spec: types.DDSRouterSpec{
		RouterSettings: types.Router{
			AllowList: []types.AllowedTopic{
				{
					Name: "rt/hello",
					Type: "string",
				},
			},
			LocalDiscoveryServer: types.Configuration{
				Type:               "local-discovery-server",
				ROSDiscoveryServer: true,
				Id:                 0,
				ListeningAddresses: []types.Address{
					{
						Ip:        "14.14.14.14",
						Port:      8080,
						Transport: "test",
					},
				},
			},
			CloudWAN: types.Configuration{
				Type: "CloudWAN",
				Id:   0,
				ListeningAddresses: []types.Address{
					{
						Ip:        "14.14.14.14",
						Port:      8080,
						Transport: "test",
					},
				},
			},
		},
	},
}

func TestCreateDDSRouter(t *testing.T) {
	if err := settings.ReadSettings(); err != nil {
		panic(err)
	}
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	if err := client.CreateDDSRouter(mockRouter); err != nil {
		panic(err)
	}
}

func TestListDDSRouter(t *testing.T) {
	if err := settings.ReadSettings(); err != nil {
		panic(err)
	}
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	ch := make(chan cluster.DDSRouterMessage)
	go func() {
		if err := client.ListDDSRouters(ch, mockRouter.Namespace); err != nil {
			panic(err)
		}
	}()

	buffer := <-ch
	if buffer.Name == mockRouter.Name && buffer.Operation == platform.Operation_Create {
		return
	}
}

func TestGetAndUpdateDDSRouter(t *testing.T) {
	client, err := getTestClient()
	if err != nil {
		t.Fatalf("error happend when get client: %v", err)
	}
	mockRouter.Spec.RouterSettings.CloudWAN.ListeningAddresses[0].Ip = "55.55.55.55"
	mockRouter.Spec.RouterSettings.AllowList = append(mockRouter.Spec.RouterSettings.AllowList, types.AllowedTopic{
		Name: "updated",
		Type: "string",
	})

	if err := client.UpdateDDSRouter(mockRouter); err != nil {
		t.Fatalf("error happend when get router: %v", err)
	}
	router, err := client.GetDDSRouter(mockRouter.Name, mockRouter.Namespace)
	if err != nil {
		t.Fatalf("error happend when get router: %v", err)
	}
	if !reflect.DeepEqual(mockRouter.Spec, router.Spec) {
		t.Fatal("update operation doesn't mutate resource!")
	}
}

func TestGetDefaultDDSRouter(t *testing.T) {
	client, err := getTestClient()
	if err != nil {
		t.Error("client cannot reachable")
	}

	router, err := client.GetDefaultDDSRouter(mockRouter.Namespace)
	if err != nil {
		t.Errorf("router not found: %v ", err)
	}
	if router.Name != mockRouter.Name {
		t.Errorf("router mismatch\nExpected: %v Actual: %v ", mockRouter.Name, router.Name)
	}
}
func TestDeleteDDSRouter(t *testing.T) {
	if err := settings.ReadSettings(); err != nil {
		panic(err)
	}
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	if err := client.DeleteDDSRouter(mockRouter.Name, mockRouter.Namespace); err != nil {
		panic(err)
	}
}

func getTestClient() (*cluster.KubeopsDynamic, error) {
	if err := settings.ReadSettings(); err != nil {
		panic(err)
	}
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		return nil, err
	}
	return client, nil
}
