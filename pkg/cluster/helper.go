package cluster

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/ipservice"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func createClientFromServiceAccount() (*kubernetes.Clientset, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}

	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func createDynamicClientFromServiceAccount() (dynamic.Interface, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}

	client, err := dynamic.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func ParseIp(ip string) (*ipservice.IP, error) {
	blocks := strings.Split(strings.Split(ip, "/")[0], ".")
	if len(blocks) < 3 {
		return nil, fmt.Errorf("%v", "error happend")
	}
	var intIp []int
	for _, block := range blocks {
		strIp, err := strconv.Atoi(block)
		if err != nil {
			return nil, err
		}
		intIp = append(intIp, strIp)
	}
	return &ipservice.IP{
		FirstBlock:  intIp[0],
		SecondBlock: intIp[1],
		ThirdBlock:  intIp[2],
		FourthBlock: intIp[3],
	}, nil
}

func ParseFor3(ip string) (int, error) {
	blocks := strings.Split(ip, ".")
	if len(blocks) < 4 {
		return 0, fmt.Errorf("%v", "error happend")
	}

	intIP, err := strconv.Atoi(blocks[2])
	if err != nil {
		return 0, err
	}
	return intIP, nil
}

func GetIPWithExclusion(exclude []int) (int, error) {
	//Create random number between 1-255 exlcude given number
	data := map[int]bool{}
	for i := 1; i < 255; i++ {
		data[i] = true
	}
	for _, element := range exclude {
		data[element] = false

	}
	for key, ip := range data {
		if ip {
			return key, nil
		}
	}
	return -1, fmt.Errorf("%v", "not found")

}
