package cluster

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	types "bitbucket.org/kaesystems/dds-controller/api/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

var DDSObserverGVR = schema.GroupVersionResource{Group: "dds.roboscale.io", Version: "v1", Resource: "ddsobservers"}

func (k *KubeopsDynamic) CreateDDSObserver(object *types.DDSObserver) error {
	object.TypeMeta = metav1.TypeMeta{
		APIVersion: "dds.roboscale.io/v1",
		Kind:       "DDSObserver",
	}
	var inInterface map[string]interface{}
	inrec, err := json.Marshal(object)
	if err != nil {
		return err
	}
	json.Unmarshal(inrec, &inInterface)

	routerObject := &unstructured.Unstructured{
		Object: inInterface,
	}
	fmt.Println(routerObject)
	_, err = k.client.Resource(DDSObserverGVR).Namespace(object.Namespace).Create(context.TODO(), routerObject, metav1.CreateOptions{})
	if err != nil {
		return err

	}
	return nil

}

func (k *KubeopsDynamic) DeleteDDSObserver(name string, namespace string) error {
	if err := k.client.Resource(DDSObserverGVR).Namespace(namespace).Delete(context.TODO(), name, metav1.DeleteOptions{}); err != nil {
		return err
	}
	return nil
}

func (k *KubeopsDynamic) GetDDSObserver(name string, namespace string) (*types.DDSObserver, error) {
	ctx := context.TODO()
	obj, err := k.client.Resource(DDSObserverGVR).Namespace(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	observer := &types.DDSObserver{}
	j, err := json.Marshal(obj.Object)
	if err != nil {
		log.Printf("Error happend: %v", err)
	}
	json.Unmarshal(j, &observer)
	if err != nil {
		return nil, err
	}
	return observer, nil
}

func (k *KubeopsDynamic) UpdateDDSObserver(observer *types.DDSObserver) error {
	ctx := context.TODO()

	obj, err := k.client.Resource(DDSObserverGVR).Namespace(observer.Namespace).Get(ctx, observer.Name, metav1.GetOptions{})
	if err != nil {
		log.Println("main error!!")
		return err
	}
	var spec map[string]interface{}
	var metadata map[string]interface{}
	if err := unstructuredDecoder(&observer.Spec, &spec); err != nil {
		log.Printf("spec transformation error:%v", err)
		return err
	}
	if err := unstructuredDecoder(&observer.ObjectMeta, &metadata); err != nil {
		log.Printf("metadata transformation error:%v", err)
		return err
	}
	obj.Object["spec"] = spec
	obj.Object["metadata"].(map[string]interface{})["annotations"] = metadata["annotations"]
	obj.Object["metadata"].(map[string]interface{})["labels"] = metadata["labels"]

	_, err = k.client.Resource(DDSObserverGVR).Namespace(observer.Namespace).Update(ctx, obj, metav1.UpdateOptions{})
	if err != nil {
		log.Println("update error!")
		return err
	}
	return nil
}
