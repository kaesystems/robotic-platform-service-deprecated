package cluster_test

import (
	"fmt"
	"testing"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/settings"
	"github.com/spf13/viper"
)

func TestKubernetesAdmin(t *testing.T) {

	// fmt.Println(string(out))
	if err := settings.ReadSettings(); err != nil {
		fmt.Println(err)
		t.Fail()
	}
	fmt.Printf("%v", viper.Get("configSource"))
	cli, err := cluster.GetKubernetesAdmin()
	if err != nil {
		fmt.Println(err)
		t.Fail()
	}
	if err := cli.CreateNamespace("test"); err != nil {
		panic(err)
	}
	if err := cli.DeleteNamespace("test"); err != nil {
		panic(err)
	}
}

// func TestKubernetesAdminDynamic(t *testing.T) {

// 	// fmt.Println(string(out))
// 	if err := settings.ReadSettings(); err != nil {
// 		fmt.Println(err)
// 		t.Fail()
// 	}
// 	fmt.Printf("%v", viper.Get("configSource"))
// 	cli, err := cluster.GetKubernetesAdminDynamic()
// 	if err != nil {
// 		fmt.Println(err)
// 		t.Fail()
// 	}
// 	if err := cli.CreateSubnet()("test"); err != nil {
// 		panic(err)
// 	}
// 	if err := cli.DeleteNamespace("test"); err != nil {
// 		panic(err)
// 	}
// }
