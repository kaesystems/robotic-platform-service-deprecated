package cluster_test

import (
	"reflect"
	"testing"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	types "bitbucket.org/kaesystems/dds-controller/api/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var mockObserver = &types.DDSObserver{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "observer-example",
		Namespace: "robosample",
	},
	Spec: types.DDSObserverSpec{
		DomainId:         0,
		PrometheusPort:   8080,
		DefaultFilter:    true,
		ElasticsearchURL: "https://elastic.robolaunch.cloud/",
	},
}

func TestCreateDDSObserver(t *testing.T) {
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	if err := client.CreateDDSObserver(mockObserver); err != nil {
		panic(err)
	}
}

func TestGetAndUpdateDDSObserver(t *testing.T) {
	client, err := getTestClient()
	if err != nil {
		t.Fatalf("error happend when get client: %v", err)
	}
	mockObserver.Spec.ElasticsearchURL = "https://nono-search.com"
	mockObserver.Spec.Filter = append(mockObserver.Spec.Filter, "filter")

	if err := client.UpdateDDSObserver(mockObserver); err != nil {
		t.Fatalf("error happend when get router: %v", err)
	}
	router, err := client.GetDDSObserver(mockObserver.Name, mockObserver.Namespace)
	if err != nil {
		t.Fatalf("error happend when get router: %v", err)
	}
	if !reflect.DeepEqual(mockObserver.Spec, router.Spec) {
		t.Fatal("update operation doesn't mutate resource!")
	}
}

func TestDeleteDDSObserver(t *testing.T) {
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	if err := client.DeleteDDSObserver(mockObserver.Name, mockObserver.Namespace); err != nil {
		panic(err)
	}
}
