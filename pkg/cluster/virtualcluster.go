package cluster

import (
	"context"
	"fmt"
	"time"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/tools/cache"
)

var VirtualCluster = schema.GroupVersionResource{Group: "tenancy.x-k8s.io", Version: "v1alpha1", Resource: "virtualclusters"}

func (k *KubeopsDynamic) CreateVirtualCluster(object map[string]interface{}) (*unstructured.Unstructured, error) {
	//Add type information to fix
	routerObject := &unstructured.Unstructured{
		Object: object,
	}
	vc, err := k.client.Resource(VirtualCluster).Namespace("default").Create(context.TODO(), routerObject, metav1.CreateOptions{})
	if err != nil {
		return nil, err

	}
	return vc, nil
}

func (k *KubeopsDynamic) GetVirtualCluster(name string) (*unstructured.Unstructured, error) {
	vc, err := k.client.Resource(VirtualCluster).Namespace("default").Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	// fmt.Printf("\n\n%v\n\n", vc)
	return vc, nil
}

func (k *KubeopsDynamic) DeleteVirtualCluster(name string) error {
	//Add type information to fix
	err := k.client.Resource(VirtualCluster).Namespace("default").Delete(context.TODO(), name, metav1.DeleteOptions{})
	if err != nil {
		return err

	}
	return nil
}

type VirtualClusterMessage struct {
	Name      string
	Status    string
	Operation platform.Operation
}

func (k *KubeopsDynamic) ListVirtualClusters(ch chan VirtualClusterMessage, namespace string) error {
	factory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(k.client, time.Minute, namespace, nil)
	informer := factory.ForResource(VirtualCluster).Informer()

	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			// distroValue := fmt.Sprintf("%v", obj.(*unstructured.Unstructured).Object["spec"].(map[string]interface{})["ros"].(map[string]interface{})["distro"])

			ch <- VirtualClusterMessage{
				Name:      fmt.Sprintf("%v", obj.(*unstructured.Unstructured).GetName()),
				Status:    fmt.Sprintf("%v", obj.(*unstructured.Unstructured).Object["status"].(map[string]interface{})["phase"]),
				Operation: platform.Operation_Create,
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			// distroValue := fmt.Sprintf("%v", old.(*unstructured.Unstructured).Object["spec"].(map[string]interface{})["ros"].(map[string]interface{})["distro"])
			// mapstructure.Decode(old.(*unstructured.Unstructured).Object, &robot)

			ch <- VirtualClusterMessage{
				Name:      fmt.Sprintf("%v", new.(*unstructured.Unstructured).GetName()),
				Status:    fmt.Sprintf("%v", new.(*unstructured.Unstructured).Object["status"].(map[string]interface{})["phase"]),
				Operation: platform.Operation_Update,
			}
		},
		DeleteFunc: func(obj interface{}) {
			// distroValue := fmt.Sprintf("%v", obj.(*unstructured.Unstructured).Object["spec"].(map[string]interface{})["ros"].(map[string]interface{})["distro"])
			// mapstructure.Decode(&robot, obj.(*unstructured.Unstructured).Object)

			ch <- VirtualClusterMessage{
				Name:      fmt.Sprintf("%v", obj.(*unstructured.Unstructured).GetName()),
				Status:    fmt.Sprintf("%v", obj.(*unstructured.Unstructured).Object["status"].(map[string]interface{})["phase"]),
				Operation: platform.Operation_Delete,
			}
		},
	})
	// ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)

	stop := make(chan struct{})
	go informer.Run(stop)
	for {
		time.Sleep(time.Second)
	}
}
