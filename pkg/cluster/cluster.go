package cluster

import (
	"fmt"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/settings"
	"github.com/spf13/viper"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type ClientType string

func (k *Kubeops) GetInternalClient() *kubernetes.Clientset {
	return k.client
}

func (k *KubeopsDynamic) GetInternalClient() dynamic.Interface {
	return k.client
}

func GetKubernetesAdmin() (*Kubeops, error) {
	//Be ensure configuration source.
	config, err := buildConfig()
	if err != nil {
		return nil, err
	}
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return &Kubeops{client: client}, nil
}

func GetKubernetesAdminDynamic() (*KubeopsDynamic, error) {
	//Be ensure configuration source.
	config, err := buildConfig()
	if err != nil {
		return nil, err
	}
	client, err := dynamic.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return &KubeopsDynamic{client: client}, nil
}

func buildConfig() (*rest.Config, error) {
	source := viper.Get("configSource")
	var config *rest.Config
	var err error
	if source != nil && source.(string) == string(settings.ConfigFile) {
		config, err = CreateFromConfigFile()
		if err != nil {
			return nil, err
		}
	} else {
		config, err = rest.InClusterConfig()
		if err != nil {
			return nil, err
		}
	}
	return config, nil
}

func CreateFromConfigFile() (*rest.Config, error) {
	caPath := viper.Get("caPath")
	insecure := viper.Get("insecure")
	kubernetesHost := viper.Get("kubernetesHost")
	kubernetesToken := viper.Get("kubernetesToken")
	var tlsConfig rest.TLSClientConfig
	if insecure != nil && insecure.(bool) {
		tlsConfig = rest.TLSClientConfig{
			Insecure: true,
		}
	} else if caPath != nil {
		tlsConfig = rest.TLSClientConfig{
			CAFile: fmt.Sprintf("%v", caPath),
		}
	} else {
		return nil, fmt.Errorf("%v", "doesn't found any tls configuration")
	}
	if kubernetesHost == nil && kubernetesToken == nil {
		return nil, fmt.Errorf("%v", "host or token doesn't specified!")
	}
	return &rest.Config{
		Host:            fmt.Sprintf("%v", kubernetesHost),
		BearerToken:     fmt.Sprintf("%v", kubernetesToken),
		TLSClientConfig: tlsConfig,
	}, nil
}

func GetKubernetesUser(token string) {

}
