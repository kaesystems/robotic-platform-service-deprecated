package cluster_test

import (
	"fmt"
	"testing"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	types "bitbucket.org/kaesystems/robolaunch/api/v1alpha1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestCreateRobot(t *testing.T) {
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	if err := client.CreateRobot(&types.Robot{
		ObjectMeta: metav1.ObjectMeta{

			Name:      "engor",
			Namespace: "robosample",
		},
		Spec: types.RobotSpec{
			ROSSpecifications: types.ROSSpecifications{
				Distro: types.ROSDistroGalactic,
			},
			AccessServices: true,
		},
	}); err != nil {
		panic(err)
	}
	// t.Error("error happend")
}

func TestListFunction(t *testing.T) {
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	ch := make(chan cluster.RobotMessage)
	go client.ListRobots(ch, "robosample")
	buffer := <-ch
	if buffer.RobotName == "engor" {
		return
	}

	// t.Error("error happend")
}

func TestGetRobot(t *testing.T) {
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	robot, err := client.GetRobot("engor", "robosample")
	if err != nil {
		panic(err)
	}
	if robot.Name != "engor" && robot.Spec.ROSSpecifications.Distro != types.ROSDistroGalactic {
		t.Fail()
	} else {
		return
	}
}

func TestUpdateRobot(t *testing.T) {
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	robot, err := client.GetRobot("engor", "robosample")
	if err != nil {
		panic(err)
	}
	fmt.Println(robot)
	robot.Spec.ROSSpecifications.Distro = types.ROSDistroMelodic
	if err := client.UpdateRobot(robot); err != nil {
		panic(err)
	}

}

func TestDeleteRobot(t *testing.T) {
	client, err := cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	if err := client.DeleteRobot("engor", "robosample"); err != nil {
		panic(err)
	}
	// t.Error("error happend")
}
