package cluster

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/tools/cache"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	types "bitbucket.org/kaesystems/robolaunch/api/v1alpha1"

	// corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

var RobotGVR = schema.GroupVersionResource{Group: "robot.roboscale.io", Version: "v1alpha1", Resource: "robots"}
var RobotRuntimeGVR = schema.GroupVersionResource{Group: "robot.roboscale.io", Version: "v1alpha1", Resource: "robotruntimes"}

type RobotMessage struct {
	RobotName          string
	Distro             string
	RobotRuntimeStatus types.RobotStatus
	Operation          platform.Operation
	ClusterName        string
	//will come from kubefed
}

func (k *KubeopsDynamic) CreateRobot(object *types.Robot) error {
	//Add type information to fix
	object.TypeMeta = metav1.TypeMeta{
		APIVersion: "robot.roboscale.io/v1alpha1",
		Kind:       "Robot",
	}
	robot := schema.GroupVersionResource{Group: "robot.roboscale.io", Version: "v1alpha1", Resource: "robots"}

	var inInterface map[string]interface{}

	inrec, err := json.Marshal(object)
	if err != nil {
		return err
	}
	json.Unmarshal(inrec, &inInterface)

	robotObj := &unstructured.Unstructured{
		Object: inInterface,
	}
	_, err = k.client.Resource(robot).Namespace(object.Namespace).Create(context.TODO(), robotObj, metav1.CreateOptions{})
	if err != nil {
		return err
	}
	return nil

}

func (k *KubeopsDynamic) DeleteRobot(name string, namespace string) error {

	robot := schema.GroupVersionResource{Group: "robot.roboscale.io", Version: "v1alpha1", Resource: "robots"}
	err := k.client.Resource(robot).Namespace(namespace).Delete(context.TODO(), name, metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	return nil

}

func (k *KubeopsDynamic) ListRobots(ch chan RobotMessage, fleetName string) error {
	robot := schema.GroupVersionResource{Group: "robot.roboscale.io", Version: "v1alpha1", Resource: "robots"}
	factory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(k.client, time.Minute, fleetName, nil)
	informer := factory.ForResource(robot).Informer()

	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			robot := &types.Robot{}
			j, err := json.Marshal(obj.(*unstructured.Unstructured).Object)
			if err != nil {
				log.Printf("Error happend: %v", err)
			}
			json.Unmarshal(j, &robot)

			// distroValue := fmt.Sprintf("%v", obj.(*unstructured.Unstructured).Object["spec"].(map[string]interface{})["ros"].(map[string]interface{})["distro"])
			ch <- RobotMessage{
				RobotName: robot.GetName(),
				Distro:    string(robot.Spec.ROSSpecifications.Distro),
				Operation: platform.Operation_Create,
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			// distroValue := fmt.Sprintf("%v", old.(*unstructured.Unstructured).Object["spec"].(map[string]interface{})["ros"].(map[string]interface{})["distro"])
			// mapstructure.Decode(old.(*unstructured.Unstructured).Object, &robot)

			robot := &types.Robot{}
			j, err := json.Marshal(new.(*unstructured.Unstructured).Object)
			if err != nil {
				log.Printf("Error happend: %v", err)
			}
			json.Unmarshal(j, &robot)

			ch <- RobotMessage{
				RobotName: robot.GetName(),
				Distro:    string(robot.Spec.ROSSpecifications.Distro),
				Operation: platform.Operation_Update,
			}
		},
		DeleteFunc: func(obj interface{}) {
			// distroValue := fmt.Sprintf("%v", obj.(*unstructured.Unstructured).Object["spec"].(map[string]interface{})["ros"].(map[string]interface{})["distro"])
			// mapstructure.Decode(&robot, obj.(*unstructured.Unstructured).Object)

			robot := &types.Robot{}
			j, err := json.Marshal(obj.(*unstructured.Unstructured).Object)
			if err != nil {
				log.Printf("Error happend: %v", err)
			}
			json.Unmarshal(j, &robot)

			ch <- RobotMessage{
				RobotName: robot.GetName(),
				Distro:    string(robot.Spec.ROSSpecifications.Distro),
				Operation: platform.Operation_Delete,
			}
		},
	})
	// ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)

	stop := make(chan struct{})
	go informer.Run(stop)
	for {
		time.Sleep(time.Second)
	}
}

func (k *KubeopsDynamic) GetRobot(name string, namespace string) (*types.Robot, error) {
	ctx := context.TODO()
	obj, err := k.client.Resource(RobotGVR).Namespace(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	robot := &types.Robot{}
	j, err := json.Marshal(obj.Object)
	if err != nil {
		log.Printf("Error happend: %v", err)
	}
	json.Unmarshal(j, &robot)

	if err != nil {
		return nil, err
	}
	return robot, nil
}

func (k *KubeopsDynamic) GetRobotRuntime(name string, namespace string) (*types.RobotRuntime, error) {
	ctx := context.TODO()
	obj, err := k.client.Resource(RobotRuntimeGVR).Namespace(namespace).Get(ctx, name+"-run", metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	robotRuntime := &types.RobotRuntime{}
	if err := unstructuredDecoder(&obj, &robotRuntime); err != nil {
		return nil, err
	}
	return robotRuntime, nil
}

func (k *KubeopsDynamic) UpdateRobot(robot *types.Robot) error {
	ctx := context.TODO()

	obj, err := k.client.Resource(RobotGVR).Namespace(robot.Namespace).Get(ctx, robot.Name, metav1.GetOptions{})
	if err != nil {
		log.Println("main error!!")
		return err
	}
	var spec map[string]interface{}
	var metadata map[string]interface{}
	log.Println(robot)
	if err := unstructuredDecoder(&robot.Spec, &spec); err != nil {
		log.Printf("spec transformation error:%v", err)
		return err
	}
	if err := unstructuredDecoder(&robot.ObjectMeta, &metadata); err != nil {
		log.Printf("metadata transformation error:%v", err)
		return err
	}
	obj.Object["spec"] = spec
	obj.Object["metadata"].(map[string]interface{})["annotations"] = metadata["annotations"]
	obj.Object["metadata"].(map[string]interface{})["labels"] = metadata["labels"]

	_, err = k.client.Resource(RobotGVR).Namespace(robot.Namespace).Update(ctx, obj, metav1.UpdateOptions{})
	if err != nil {
		log.Println("update error!")
		return err
	}
	return nil
}

func unstructuredDecoder(in interface{}, out interface{}) error {
	j, err := json.Marshal(in)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(j, &out); err != nil {
		return err
	}
	return nil
}
