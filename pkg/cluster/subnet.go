package cluster

import (
	"context"
	"fmt"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/ipservice"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

var Subnet = schema.GroupVersionResource{Group: "kubeovn.io", Version: "v1", Resource: "subnets"}

func (k *KubeopsDynamic) CreateSubnet(subnet map[string]interface{}) error {
	subnetObject := &unstructured.Unstructured{
		Object: subnet,
	}
	_, err := k.client.Resource(Subnet).Create(context.TODO(), subnetObject, metav1.CreateOptions{})
	if err != nil {
		return err
	}
	return nil
}

func (k *KubeopsDynamic) DeleteSubnet(name string) error {

	if err := k.client.Resource(Subnet).Delete(context.TODO(), name, metav1.DeleteOptions{}); err != nil {
		return err
	}

	return nil
}
func (k *KubeopsDynamic) listSubnet() (*unstructured.UnstructuredList, error) {
	subnets, err := k.client.Resource(Subnet).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	return subnets, nil
}

func (k *KubeopsDynamic) GenerateIP() (*ipservice.IP, error) {
	//Block predifined
	subnets, err := k.listSubnet()
	if err != nil {
		return nil, err
	}
	var taken []int
	for _, subnet := range subnets.Items {
		block, err := ParseFor3(fmt.Sprintf("%v", subnet.Object["spec"].(map[string]interface{})["cidrBlock"]))
		if err != nil {
			return nil, err
		}
		taken = append(taken, block)
	}
	validBlock, err := GetIPWithExclusion(taken)
	if err != nil {
		return nil, err
	}
	return &ipservice.IP{FirstBlock: 10, SecondBlock: 10, ThirdBlock: validBlock, FourthBlock: 0}, nil
}
