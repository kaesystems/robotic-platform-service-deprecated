package ipservice

import "strconv"

type IP struct {
	FirstBlock  int
	SecondBlock int
	ThirdBlock  int
	FourthBlock int
}

func (i *IP) GetIP() string {
	return strconv.Itoa(i.FirstBlock) + "." + strconv.Itoa(i.SecondBlock) + "." + strconv.Itoa(i.ThirdBlock) + "." + strconv.Itoa(i.FourthBlock)
}

func (i *IP) GetIPBlock() string {
	return i.GetIP() + "/24"
}

func (i *IP) CopyIP() *IP {
	return &IP{
		FirstBlock:  i.FirstBlock,
		SecondBlock: i.SecondBlock,
		ThirdBlock:  i.ThirdBlock,
		FourthBlock: i.FourthBlock,
	}
}
