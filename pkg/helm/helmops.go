package helm

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"time"

	"gopkg.in/yaml.v3"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/release"
)

func ListHelmReleases(actionConfig *action.Configuration) error {
	cmd := action.NewList(actionConfig)
	cmd.AllNamespaces = true
	cmd.StateMask = action.ListAll
	cmd.Limit = 200
	releases, err := cmd.Run()
	if err != nil {
		return err
	}
	for _, v := range releases {
		fmt.Printf("Chart Name: %v, Chart Namespace: %v\n", v.Name, v.Namespace)
	}
	fmt.Println(len(releases))
	return nil
}

func CreateHelmReleases(chart *chart.Chart, actionConfig *action.Configuration, namespace string, releaseName string) (*release.Release, error) {
	cmd := action.NewInstall(actionConfig)
	cmd.Namespace = namespace
	cmd.ReleaseName = releaseName
	release, err := cmd.Run(chart, chart.Values)
	if err != nil {
		return nil, err
	}
	fmt.Println(release)
	return release, nil
}

func DeleteRelease(actionConfig *action.Configuration, releaseName string) error {
	cmd := action.NewUninstall(actionConfig)
	cmd.Timeout = time.Duration(10) * time.Second
	_, err := cmd.Run(releaseName)
	if err != nil {
		return nil
	}
	return err
}

func LoadFromRepo(url string, repoName string) (*chart.Chart, error) {
	chartURL, err := findInIndex(url, repoName)
	if err != nil {
		return nil, err
	}
	resp, err := http.Get(*chartURL)
	if err != nil {
		return nil, err
	}

	chart, err := loader.LoadArchive(resp.Body)
	if err != nil {
		return nil, err
	}
	return chart, nil
}

func findInIndex(url string, repoName string) (*string, error) {
	resp, err := http.Get(url + "/index.yaml")
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	parsedIndex := map[string]interface{}{}
	if err := yaml.Unmarshal(body, parsedIndex); err != nil {
		return nil, err
	}
	if parsedIndex["entries"].(map[string]interface{})[repoName] == nil {
		return nil, errors.New("not found")
	}

	//FIXME: Change that part later.
	versions := parsedIndex["entries"].(map[string]interface{})[repoName]
	downloadUrl := fmt.Sprintf("%v", versions.([]interface{})[0].(map[string]interface{})["urls"].([]interface{})[0])
	if isLink(downloadUrl) {
		return &downloadUrl, nil
	}
	downloadUrl = url + "/" + downloadUrl
	fmt.Println(downloadUrl)
	return &downloadUrl, nil
}

func isLink(link string) bool {
	re := regexp.MustCompile(`^(?:https?://)`)
	fmt.Println(re.MatchString(link))
	return re.MatchString(link)
}
