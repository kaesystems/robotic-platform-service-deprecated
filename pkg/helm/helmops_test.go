package helm_test

import (
	"fmt"
	"testing"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/helm"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/settings"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func TestHelmRelease(t *testing.T) {
	if err := settings.ReadSettings(); err != nil {
		panic(err)
	}

	clusterConf, err := cluster.CreateFromConfigFile()
	if err != nil {
		panic(err)
	}
	clusterConf.TLSClientConfig = rest.TLSClientConfig{Insecure: true}
	client, err := kubernetes.NewForConfig(clusterConf)
	if err != nil {
		panic(err)
	}
	// store := StorageForConfigMaps("openebs", client)
	actionTest, err := helm.NewActionConfig(helm.StorageForSecrets, clusterConf, client, "")
	if err != nil {
		panic(err)
	}
	// test := NewConfigFlagsFromCluster("openebs")
	if err := helm.ListHelmReleases(actionTest); err != nil {
		fmt.Println(err)
		t.Fail()
	}
	t.Fail()

}

func TestCreateHelmRelease(t *testing.T) {
	//Lets debug chart together
	// readChart, err := loader.Load("fakechart")
	// if err != nil {
	// 	panic(err)
	// }
	readChart, err := helm.LoadFromRepo("http://34.224.78.190:32248", "submariner-k8s-broker")
	if err != nil {
		panic(err)
	}
	actionConfig := helm.GetAction("default")
	_, err = helm.CreateHelmReleases(readChart, actionConfig, "default", "tester-from-repo")
	if err != nil {
		fmt.Println(err)
		t.Fail()
	}
}

func TestLoadFromRepo(t *testing.T) {
	_, err := helm.LoadFromRepo("https://charts.bitnami.com/bitnami", "airflow")
	if err != nil {
		panic(err)
	}
}

func TestDeleteChart(t *testing.T) {
	actionConfig := helm.GetAction("default")
	if err := helm.DeleteRelease(actionConfig, "tester-from-repo"); err != nil {
		panic(err)
	}
}

// func TestIsLink(t *testing.T) {
// 	expectedTrue := helm.IsLink("https://engin.com")
// 	if !expectedTrue {
// 		t.Fail()
// 	}

// 	expectedFalse := helm.IsLink("")
// 	if expectedFalse {
// 		t.Fail()
// 	}
// }
