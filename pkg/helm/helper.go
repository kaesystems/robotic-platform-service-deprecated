package helm

import (
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/settings"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/kube"
	"helm.sh/helm/v3/pkg/storage"
	"helm.sh/helm/v3/pkg/storage/driver"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	log "k8s.io/klog/v2"
)

type StorageForDriver func(namespace string, clientset *kubernetes.Clientset) *storage.Storage

func NewConfigFlagsFromCluster(namespace string) genericclioptions.RESTClientGetter {
	impersonateGroup := []string{}
	conf, err := cluster.CreateFromConfigFile()
	if err != nil {
		panic(err)
	}
	// CertFile and KeyFile must be nil for the BearerToken to be used for authentication and authorization instead of the pod's service account.
	configFlags := &genericclioptions.ConfigFlags{
		Insecure:         boolptr(true),
		Timeout:          stringptr("0"),
		Namespace:        stringptr(namespace),
		APIServer:        stringptr(conf.Host),
		BearerToken:      stringptr(conf.BearerToken),
		ImpersonateGroup: &impersonateGroup,
	}
	return &configForCluster{
		config:         conf,
		discoveryBurst: 100,
		ConfigFlags:    configFlags,
	}
}

func stringptr(val string) *string {
	return &val
}

func boolptr(val bool) *bool {
	return &val
}

func GetAction(namespace string) *action.Configuration {
	if err := settings.ReadSettings(); err != nil {
		panic(err)
	}

	clusterConf, err := cluster.CreateFromConfigFile()
	if err != nil {
		panic(err)
	}
	clusterConf.TLSClientConfig = rest.TLSClientConfig{Insecure: true}
	client, err := kubernetes.NewForConfig(clusterConf)
	if err != nil {
		panic(err)
	}
	// store := StorageForConfigMaps("openebs", client)
	actionTest, err := NewActionConfig(StorageForSecrets, clusterConf, client, "default")
	if err != nil {
		panic(err)
	}
	return actionTest
}

func NewActionConfig(storageForDriver StorageForDriver, config *rest.Config, clientset *kubernetes.Clientset, namespace string) (*action.Configuration, error) {
	actionConfig := new(action.Configuration)
	store := storageForDriver(namespace, clientset)
	restClientGetter := NewConfigFlagsFromCluster(namespace)
	actionConfig.RESTClientGetter = restClientGetter
	actionConfig.KubeClient = kube.New(restClientGetter)
	actionConfig.Releases = store
	actionConfig.Log = log.Infof
	return actionConfig, nil
}

// StorageForSecrets returns a storage using the Secret driver.
func StorageForSecrets(namespace string, clientset *kubernetes.Clientset) *storage.Storage {
	d := driver.NewSecrets(clientset.CoreV1().Secrets(namespace))
	d.Log = log.Infof
	return storage.Init(d)
}

// StorageForConfigMaps returns a storage using the ConfigMap driver.
func StorageForConfigMaps(namespace string, clientset *kubernetes.Clientset) *storage.Storage {
	d := driver.NewConfigMaps(clientset.CoreV1().ConfigMaps(namespace))
	d.Log = log.Infof
	return storage.Init(d)
}

// StorageForMemory returns a storage using the Memory driver.
func StorageForMemory(_ string, _ *kubernetes.Clientset) *storage.Storage {
	d := driver.NewMemory()
	return storage.Init(d)
}

type configForCluster struct {
	config         *rest.Config
	discoveryBurst int
	*genericclioptions.ConfigFlags
}
