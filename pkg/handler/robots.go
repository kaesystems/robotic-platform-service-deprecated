package handler

import (
	"context"
	"log"
	"time"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	robotTypes "bitbucket.org/kaesystems/robolaunch/api/v1alpha1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (s Server) ListRobots(in *platform.FleetRequest, stream platform.Robot_ListRobotsServer) error {
	ch := make(chan cluster.RobotMessage)
	go func() {
		if err := adminDynamic.ListRobots(ch, in.Name); err != nil {
			time.Sleep(time.Second * 1)
		}
	}()
	for {
		buffer := <-ch
		stream.Send(&platform.ListRobotResponse{
			Robot: &platform.ListRobotResponse_RobotSummary{
				Name:        buffer.RobotName,
				Distro:      buffer.Distro,
				ClusterName: "not-defined",
			},
			Operation: buffer.Operation,
		})
	}
}

func (s Server) GetRobot(ctx context.Context, in *platform.GetRobotRequest) (*platform.RobotResponse, error) {
	robot, err := adminDynamic.GetRobot(in.GetName(), in.GetFleet().Name)
	workspaces := []*platform.Workspace{}
	robotRuntimeStatus := platform.RobotRuntimeStatus{}
	robotConfigStatus := platform.RobotConfigStatus{}

	if err != nil {
		err = status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	cloudIde := false
	if robot.Spec.ROSSpecifications.CloudIDE == robotTypes.CloudIDEOptionEnabled {
		cloudIde = true
	}

	if err := interfaceConverter(&robot.Status.RuntimeStatus, &robotRuntimeStatus); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	if err := interfaceConverter(&robot.Status.ConfigStatus, &robotConfigStatus); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}

	for _, v := range robot.Spec.ROSSpecifications.Workspaces {
		ws := &platform.Workspace{}
		if err := interfaceConverter(v, ws); err != nil {
			err = status.Error(codes.Internal, err.Error())
			return nil, err
		}
		workspaces = append(workspaces, ws)
	}
	return &platform.RobotResponse{
		Name: robot.GetName(),
		Fleet: &platform.FleetRequest{
			Name: robot.GetNamespace(),
		},
		Distro:             string(robot.Spec.ROSSpecifications.Distro),
		CloudIde:           cloudIde,
		WorkspacesPath:     robot.Spec.ROSSpecifications.WorkspacesPath,
		Workspaces:         workspaces,
		RobotRuntimeStatus: &robotRuntimeStatus,
	}, nil
}

func (s Server) CreateRobot(ctx context.Context, in *platform.RobotRequest) (*platform.RobotRequest, error) {
	var robotSpec robotTypes.ROSSpecifications
	if err := interfaceConverter(in, &robotSpec); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	robot := &robotTypes.Robot{
		ObjectMeta: metav1.ObjectMeta{
			Name:      in.Name,
			Namespace: in.GetFleet().Name,
		},
		Spec: robotTypes.RobotSpec{
			ROSSpecifications: robotSpec,
		},
	}
	if err := adminDynamic.CreateRobot(robot); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	return in, nil
}

func (s Server) UpdateRobot(ctx context.Context, in *platform.RobotRequest) (*platform.RobotResponse, error) {
	var rosSpec robotTypes.ROSSpecifications
	if err := interfaceConverter(in, &rosSpec); err != nil {
		log.Fatalf("Error happaned on type conversion: %v\n", err)
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	log.Printf("\n%v\n", rosSpec)
	robot := &robotTypes.Robot{
		ObjectMeta: metav1.ObjectMeta{
			Name:      in.Name,
			Namespace: in.GetFleet().Name,
		},
		Spec: robotTypes.RobotSpec{
			ROSSpecifications: rosSpec,
		},
	}
	if err := adminDynamic.UpdateRobot(robot); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}

	return &platform.RobotResponse{}, nil
}

func (s Server) DeleteRobot(ctx context.Context, in *platform.GetRobotRequest) (*platform.Empty, error) {
	if err := adminDynamic.DeleteRobot(in.GetName(), in.GetFleet().GetName()); err != nil {
		err = status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	return &platform.Empty{}, nil
}
