package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	routerTypes "bitbucket.org/kaesystems/dds-controller/api/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/reflect/protoreflect"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (s Server) CreateObserver(ctx context.Context, in *platform.Observer) (*platform.Observer, error) {
	//define spec variable to copy proto message
	var observerSpec routerTypes.DDSObserverSpec

	if err := fromProtoMessage(in, &observerSpec); err != nil {
		err = status.Error(codes.InvalidArgument, err.Error())
		return nil, err
	}
	//FIXME: array copy error should be fixed
	observer := routerTypes.DDSObserver{
		ObjectMeta: metav1.ObjectMeta{
			Name:      in.GetName(),
			Namespace: in.GetFleet().GetName(),
		},
		Spec: observerSpec,
	}
	if err := adminDynamic.CreateDDSObserver(&observer); err != nil {
		err = status.Error(codes.AlreadyExists, err.Error())
		return nil, err
	}
	return in, nil
}

func (s Server) GetObserver(ctx context.Context, in *platform.GetObserverRequest) (*platform.Observer, error) {
	var observer platform.Observer

	resp, err := adminDynamic.GetDDSObserver(in.GetName(), in.GetFleet().GetName())
	if err != nil {
		err = status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	//TODO: Create adapter class for converting

	if err := toProtoMessage(resp.Spec, &observer); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	observer.Name = in.GetName()
	observer.Fleet = in.GetFleet()
	return &observer, nil
}

func (s Server) DeleteObserver(ctx context.Context, in *platform.GetObserverRequest) (*platform.Empty, error) {
	if err := adminDynamic.DeleteDDSObserver(in.GetName(), in.GetFleet().GetName()); err != nil {
		err = status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	return &platform.Empty{}, nil
}

func (s Server) UpdateObserver(ctx context.Context, in *platform.Observer) (*platform.Observer, error) {
	var observerSettings routerTypes.DDSObserverSpec

	if err := fromProtoMessage(in, &observerSettings); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	//FIXME: array copy error should be fixed
	observer := routerTypes.DDSObserver{
		ObjectMeta: metav1.ObjectMeta{
			Name:      in.GetName(),
			Namespace: in.GetFleet().GetName(),
		},
		Spec: observerSettings,
	}
	if err := adminDynamic.UpdateDDSObserver(&observer); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	return in, nil
}

func fromProtoMessage(from protoreflect.ProtoMessage, to interface{}) error {
	robotConv, err := protojson.Marshal(from)
	if err != nil {
		return err
	}
	fmt.Println(string(robotConv))
	if err := json.Unmarshal(robotConv, to); err != nil {
		log.Fatalf("Coversion error detected: %v\n", err)
		return err
	}
	return nil
}

func toProtoMessage(from interface{}, to protoreflect.ProtoMessage) error {
	robotConv, err := json.Marshal(from)
	if err != nil {
		return err
	}
	fmt.Println(string(robotConv))
	if err := protojson.Unmarshal(robotConv, to); err != nil {
		log.Fatalf("Coversion error detected: %v\n", err)
		return err
	}
	return nil
}
