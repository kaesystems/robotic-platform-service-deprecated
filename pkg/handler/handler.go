package handler

import (
	"log"
	"net"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"

	"google.golang.org/grpc"
)

const PORT = ":8080"

var grpcHandler *grpc.Server
var adminClient *cluster.Kubeops
var adminDynamic *cluster.KubeopsDynamic

type Server struct {
	platform.UnimplementedRobotServer
}

func initServer() {
	grpcHandler = grpc.NewServer()
	platform.RegisterRobotServer(grpcHandler, &Server{})
}

func StartServer() {
	initServer()
	var err error
	adminClient, err = cluster.GetKubernetesAdmin()
	if err != nil {
		panic(err)
	}
	adminDynamic, err = cluster.GetKubernetesAdminDynamic()
	if err != nil {
		panic(err)
	}
	lis, err := net.Listen("tcp", PORT)
	if err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
	if err := grpcHandler.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
