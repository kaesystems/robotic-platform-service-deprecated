package handler

import (
	"context"
	"time"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/helper"
	wf "bitbucket.com/kaesystems/robotic-platform-service/pkg/workflow"
	"go.temporal.io/sdk/client"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const TEMPORAL_CLIENT = "34.224.78.190:30494"

//Create cluster will start cluster creation function on temporal
func (s Server) CreateCluster(ctx context.Context, in *platform.ClusterRequest) (*platform.ClusterWorkflowTransaction, error) {
	headers, _ := metadata.FromIncomingContext(ctx)
	userInfo, err := helper.ParseUserInfo(headers)
	if err != nil {
		err = status.Error(codes.PermissionDenied, err.Error())
		return nil, err
	}
	temporalClient, err := client.NewClient(client.Options{
		HostPort: TEMPORAL_CLIENT, // that field should be managed from IaC principles.
	})
	if err != nil {
		err = status.Error(codes.Unavailable, err.Error())
		return nil, err
	}
	defer temporalClient.Close()
	workflowOptions := client.StartWorkflowOptions{
		ID:        in.Name,
		TaskQueue: wf.VirtualClusterQueue,
	}
	we, err := temporalClient.ExecuteWorkflow(context.Background(), workflowOptions, wf.CreateVirtualCluster, wf.VirtualClustersParamater{
		Name:            in.GetName(),
		ClusterTemplate: "keycloak-cluster",
		Username:        userInfo.Username, // that should be came from auth metadata
	})
	if err != nil {
		err = status.Error(codes.Aborted, err.Error())
		return nil, err
	}
	return &platform.ClusterWorkflowTransaction{
		Name:  we.GetID(),
		RunId: we.GetRunID(),
	}, nil
}

func (s Server) GetCluster(ctx context.Context, in *platform.ClusterWorkflowTransaction) (*platform.ClusterResponse, error) {
	temporalClient, err := client.NewClient(client.Options{
		HostPort: TEMPORAL_CLIENT, // that field should be managed from IaC principles.
	})
	if err != nil {
		err := status.Error(codes.Unavailable, err.Error())
		return nil, err
	}

	result, err := temporalClient.DescribeWorkflowExecution(context.TODO(), in.GetName(), in.GetRunId())
	if err != nil {
		err := status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	return &platform.ClusterResponse{
		Name: in.Name,
		Ip:   result.WorkflowExecutionInfo.Status.String(),
	}, nil
}

func (s Server) DeleteCluster(ctx context.Context, in *platform.ClusterRequest) (*platform.Empty, error) {
	if err := adminDynamic.DeleteVirtualCluster(in.GetName()); err != nil {
		err := status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	if err := adminDynamic.DeleteSubnet("subnet-" + in.GetName()); err != nil {
		err := status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	return &platform.Empty{}, nil
}

func (s Server) ListClusters(in *platform.Empty, stream platform.Robot_ListClustersServer) error {
	ch := make(chan cluster.VirtualClusterMessage)
	// var buffer string
	go func() {
		if err := adminDynamic.ListVirtualClusters(ch, "default"); err != nil {
			time.Sleep(time.Second * 1)
		}
	}()
	for {
		buffer := <-ch
		stream.Send(&platform.ClusterResponseSummary{
			Cluster: &platform.ClusterResponse{
				Name:   buffer.Name,
				Status: buffer.Status,
			},
			Operation: buffer.Operation,
		})
	}
}
