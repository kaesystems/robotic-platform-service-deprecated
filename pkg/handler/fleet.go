package handler

import (
	"context"
	"time"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s Server) CreateFleet(ctx context.Context, in *platform.FleetRequest) (*platform.FleetResponse, error) {
	if err := adminClient.CreateNamespace(in.GetName()); err != nil {
		err = status.Error(codes.AlreadyExists, err.Error())
		return nil, err
	}
	return &platform.FleetResponse{
		Name: in.GetName(),
	}, nil
}

func (s Server) ListFleets(in *platform.Empty, stream platform.Robot_ListFleetsServer) error {
	ch := make(chan cluster.FleetMessage)
	// var buffer string
	go func() {
		if err := adminClient.ListNamespace("hey", ch); err != nil {
			time.Sleep(time.Second * 1)
		}
	}()
	for {
		buffer := <-ch
		stream.Send(&platform.FleetList{
			Fleet: &platform.FleetRequest{
				Name: buffer.FleetName,
			},
			Operation: buffer.Operation,
		})
	}
}

func (s Server) DeleteFleet(ctx context.Context, in *platform.FleetRequest) (*platform.Empty, error) {
	if err := adminClient.DeleteNamespace(in.GetName()); err != nil {
		err = status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	return &platform.Empty{}, nil
}
