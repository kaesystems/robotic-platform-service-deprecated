package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	platform "bitbucket.com/kaesystems/robotic-platform-service/api"
	"bitbucket.com/kaesystems/robotic-platform-service/pkg/cluster"
	routerTypes "bitbucket.org/kaesystems/dds-controller/api/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (s Server) CreateRouter(ctx context.Context, in *platform.Router) (*platform.Router, error) {
	//define meta first
	var routerSettings routerTypes.Router
	var listeningWan []routerTypes.Address
	var listeningLocal []routerTypes.Address

	if err := interfaceConverter(in, &routerSettings); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	//FIXME: array copy error should be fixed
	if err := interfaceConverter(in.CloudWAN.ListeningAddresses, &listeningWan); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	if err := interfaceConverter(in.CloudWAN.ListeningAddresses, &listeningLocal); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}

	router := routerTypes.DDSRouter{
		ObjectMeta: metav1.ObjectMeta{
			Name:      in.GetName(),
			Namespace: in.GetFleet().GetName(),
		},
		Spec: routerTypes.DDSRouterSpec{
			RouterSettings: routerSettings,
		},
	}
	router.Spec.RouterSettings.CloudWAN.ListeningAddresses = listeningWan
	router.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses = listeningLocal
	if err := adminDynamic.CreateDDSRouter(&router); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	return in, nil
}

func (s Server) GetRouter(ctx context.Context, in *platform.GetRouterRequest) (*platform.Router, error) {
	var router platform.Router
	var localAddresses []*platform.Router_ListeningAddress
	var cloudAdresses []*platform.Router_ListeningAddress

	resp, err := adminDynamic.GetDDSRouter(in.GetName(), in.GetFleet().GetName())
	if err != nil {
		err = status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	//TODO: Create adapter class for converting

	if err := interfaceConverter(resp.Spec.RouterSettings, &router); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	if err := interfaceConverter(resp.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses, &localAddresses); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	if err := interfaceConverter(resp.Spec.RouterSettings.CloudWAN.ListeningAddresses, &cloudAdresses); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	router.CloudWAN.ListeningAddresses = cloudAdresses
	router.LocalDiscoveryServer.ListeningAddresses = localAddresses
	router.Name = resp.Name
	router.Fleet = &platform.FleetRequest{
		Name: resp.Namespace,
	}
	return &router, nil
}

func (s Server) DeleteRouter(ctx context.Context, in *platform.GetRouterRequest) (*platform.Empty, error) {
	if err := adminDynamic.DeleteDDSRouter(in.GetName(), in.GetFleet().GetName()); err != nil {
		err = status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	return &platform.Empty{}, nil
}

func (s Server) UpdateRouter(ctx context.Context, in *platform.Router) (*platform.Router, error) {
	var routerSettings routerTypes.Router
	var listeningWan []routerTypes.Address
	var listeningLocal []routerTypes.Address

	if err := interfaceConverter(in, &routerSettings); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	//FIXME: array copy error should be fixed
	if err := interfaceConverter(in.CloudWAN.ListeningAddresses, &listeningWan); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	if err := interfaceConverter(in.CloudWAN.ListeningAddresses, &listeningLocal); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}

	router := routerTypes.DDSRouter{
		ObjectMeta: metav1.ObjectMeta{
			Name:      in.GetName(),
			Namespace: in.GetFleet().GetName(),
		},
		Spec: routerTypes.DDSRouterSpec{
			RouterSettings: routerSettings,
		},
	}
	router.Spec.RouterSettings.CloudWAN.ListeningAddresses = listeningWan
	router.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses = listeningLocal
	if err := adminDynamic.UpdateDDSRouter(&router); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	return in, nil
}

func (s Server) ListRouter(in *platform.FleetRequest, stream platform.Robot_ListRouterServer) error {
	ch := make(chan cluster.DDSRouterMessage)
	go func() {
		if err := adminDynamic.ListDDSRouters(ch, in.GetName()); err != nil {
			time.Sleep(time.Second * 1)
		}
	}()
	for {
		buffer := <-ch
		stream.Send(&platform.RouterSummary{
			Router: &platform.RouterSummary_Router{
				Name:       buffer.Name,
				Fleet:      buffer.Fleet,
				InternalIP: buffer.InternalIP,
				ExternalIP: buffer.ExternalIP,
			},
			Operation: buffer.Operation,
		})
	}
}

func (s Server) GetDefaultRouter(ctx context.Context, in *platform.FleetRequest) (*platform.Router, error) {
	var router platform.Router
	var localAddresses []*platform.Router_ListeningAddress
	var cloudAdresses []*platform.Router_ListeningAddress
	resp, err := adminDynamic.GetDefaultDDSRouter(in.GetName())
	if err != nil {
		err = status.Error(codes.NotFound, err.Error())
		return nil, err
	}
	//TODO: Create adapter class for converting

	if err := interfaceConverter(resp.Spec.RouterSettings, &router); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	if err := interfaceConverter(resp.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses, &localAddresses); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	if err := interfaceConverter(resp.Spec.RouterSettings.CloudWAN.ListeningAddresses, &cloudAdresses); err != nil {
		err = status.Error(codes.Internal, err.Error())
		return nil, err
	}
	router.CloudWAN.ListeningAddresses = cloudAdresses
	router.LocalDiscoveryServer.ListeningAddresses = localAddresses
	router.Name = resp.Name
	router.Fleet = &platform.FleetRequest{
		Name: resp.Namespace,
	}
	return &router, nil
}

func interfaceConverter(from interface{}, to interface{}) error {
	robotConv, err := json.Marshal(from)
	if err != nil {
		return err
	}
	fmt.Println(string(robotConv))
	if err := json.Unmarshal(robotConv, to); err != nil {
		log.Fatalf("Coversion error detected: %v\n", err)
		return err
	}
	return nil
}
