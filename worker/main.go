package main

import (
	"log"

	"bitbucket.com/kaesystems/robotic-platform-service/pkg/settings"
	wf "bitbucket.com/kaesystems/robotic-platform-service/pkg/workflow"
	"github.com/spf13/viper"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

func main() {
	temporalClient, err := client.NewClient(client.Options{
		// HostPort: "100.25.198.115:31255", // thats main cluster temporal IP
		HostPort: "34.224.78.190:30494", // thats main cluster temporal IP

	})
	if err != nil {
		panic(err)
	}
	if err := settings.ReadSettings(); err != nil {
		panic(err)
	}
	log.Printf("Cluster Settings readed from %v", viper.Get("configSource"))
	defer temporalClient.Close()
	w := worker.New(temporalClient, wf.VirtualClusterQueue, worker.Options{})
	w.RegisterWorkflow(wf.CreateVirtualCluster)
	w.RegisterActivity(wf.DeployVirtualCluster)
	w.RegisterActivity(wf.CheckStatusVirtualCluster)
	w.RegisterActivity(wf.DeployAdminRole)
	w.RegisterActivity(wf.AssignRoleToUser)
	w.RegisterActivity(wf.CreateSubnet)
	if err := w.Run(worker.InterruptCh()); err != nil {
		panic(err)
	}

}
